
var exec = require('cordova/exec');

var linkedin = {
    
    createSessionWithAuth:function(success,error) {                
        exec(success, error, "LinkedInPlugin", "createSessionWithAuth", []);
    },        
    getProfile:function(success,error) {                
        exec(success, error, "LinkedInPlugin", "getProfile", []);
    },        
    viewMyProfile:function(success, error) {
        exec(success, error, "LinkedInPlugin", "viewMyProfile");
    },    
    viewMemberProfile:function(memberId, success, error) {
        exec(success, error, "LinkedInPlugin", "viewMemberProfile", [memberId]);
    },
    hasLinkedInApp:function(success) {
        exec(success, null, "LinkedInPlugin", "hasLinkedInApp", []);
    }    
    
};

module.exports = linkedin;
