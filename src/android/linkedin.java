
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.listeners.ApiResponse;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.APIHelper;
import java.lang.reflect.Method;

/**
 * This class echoes a string called from JavaScript.
 */
public class linkedin extends CordovaPlugin {
    

    @Override
    public boolean execute(String action, JSONArray args, org.apache.cordova.CallbackContext callbackContext) throws JSONException {

        // Store a reference to the current activity 
        final Activity thisActivity = this;
        
        try {
            
            if (action.equals("createSessionWithAuth")) {

                Scope scope = Scope.build(Scope.R_BASICPROFILE, Scope.W_SHARE);
                
                LISessionManager.getInstance(getApplicationContext()).init(thisActivity, scope, new AuthListener() {
                    @Override
                    public void onAuthSuccess() {
                        // Authentication was successful.  You can now do
                        // other calls with the SDK.
                        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, true));
                    }

                    @Override
                    public void onAuthError(LIAuthError error) {
                        // Handle authentication errors
                         callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR));
                    }
                }, true);
            }
            
            if (action.equals("hasLinkedInApp")) {
                boolean installed = appInstalledOrNot("com.linkedin.android");  
                callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, installed);
                return true;
            }      
                                                 
            if (action.equals("viewMemberProfile")) {
                
                // A member ID value to open
                final String targetID = args.getString(0);

                DeepLinkHelper deepLinkHelper = DeepLinkHelper.getInstance();

                // Open the target LinkedIn member's profile
                deepLinkHelper.openOtherProfile(thisActivity, targetID, new DeeplinkListener() {
                    @Override
                    public void onDeepLinkSuccess() {
                        // Successfully sent user to LinkedIn app
                        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, true));
                    }

                    @Override
                    public void onDeepLinkError(LiDeepLinkError error) {
                        // Error sending user to LinkedIn app
                        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, false));
                    }
                });                          
                return true;
            }
                                                 
            if(action.equals("getProfile"))
            {
                String url = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name)";

                APIHelper apiHelper = APIHelper.getInstance(getApplicationContext());
                apiHelper.getRequest(this, url, new ApiListener() {
                    @Override
                    public void onApiSuccess(ApiResponse apiResponse) {
                        // Success!
                        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, apiResponse));
                    }

                    @Override
                    public void onApiError(LIApiError liApiError) {
                        // Error making GET request!
                        callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.ERROR)); 
                    }
                });

            }
        }
        catch (Exception ex)
        {
            callbackContext.error(ex.getMessage());
        }
        return false;

    }

                                                 
    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed ;
    }                                                 



}