//
//  Created by fluxloop on 18/08/2016
//

#import <linkedin-sdk/LISDK.h>
#import <Cordova/CDVViewController.h>
#import <Cordova/CDVScreenOrientationDelegate.h>
#import "LinkedInPlugin.h"


@implementation LinkedInPlugin


- (void)pluginInitialize
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(finishLaunching:) name:UIApplicationDidFinishLaunchingNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidResume:) name: UIApplicationDidBecomeActiveNotification object: nil];
}

- (void)finishLaunching:(NSNotification *)notification
{
    NSLog(@"finishLaunching");
}

- (void)applicationDidResume:(UIApplication *)application
{
    NSLog(@"applicationDidResume");
    // Check for valid session with access token and fire event with accessToken
    if ([LISDKSessionManager hasValidSession]) {
        NSString *accessToken = [[[LISDKSessionManager sharedInstance] session].accessToken description];
        NSLog(@"accessToken: %@",accessToken);
        [self.commandDelegate evalJs:[NSString stringWithFormat:@"cordova.fireDocumentEvent('lnValidSession', ['%@']); ", accessToken]];
    }
    else {
        // Create new session based on last access token
        LISDKAccessToken *lastAccessToken = [self lastAccessToken];
        if (lastAccessToken)[LISDKSessionManager createSessionWithAccessToken:lastAccessToken];
    }
}


-(void)viewMemberProfile:(CDVInvokedUrlCommand*)command{
    NSString* memberId = [command argumentAtIndex:0];
    if (memberId) {
        DeeplinkSuccessBlock success = ^(NSString *returnState) {
            NSLog(@"Success with returned state: %@",returnState);
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:returnState];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        };
        DeeplinkErrorBlock error = ^(NSError *error, NSString *returnState) {
            NSLog(@"Error with returned state: %@", returnState);
            NSLog(@"Error %@", error);
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.description];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        };
        [[LISDKDeeplinkHelper sharedInstance] viewOtherProfile:memberId withState:@"viewMemberProfileButton" showGoToAppStoreDialog:YES success:success error:error];
    }
}

-(void)viewMyProfile:(CDVInvokedUrlCommand*)command{
    DeeplinkSuccessBlock success = ^(NSString *returnState) {
        NSLog(@"Success with returned state: %@",returnState);
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:returnState];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    };
    DeeplinkErrorBlock error = ^(NSError *error, NSString *returnState) {
        NSLog(@"Error with returned state: %@", returnState);
        NSLog(@"Error %@", error);
        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.description];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    };
    
    [[LISDKDeeplinkHelper sharedInstance] viewCurrentProfileWithState:@"viewMyProfileButton" showGoToAppStoreDialog:YES success:success error:error];
}

-(void)getProfile:(CDVInvokedUrlCommand*)command {
    
    NSString *resourceText = @"https://www.linkedin.com/v1/people/~:(id,firstName,lastName,maiden-name,headline,industry,summary,specialties,num-connections,email-address,picture-urls::(original),public-profile-url,location:(),positions:())";
    
    [[LISDKAPIHelper sharedInstance] apiRequest:resourceText
                                         method:@"GET"
                                           body:[@"" dataUsingEncoding:NSUTF8StringEncoding]
                                        success:^(LISDKAPIResponse *response) {
                                            NSLog(@"success called %@", response.data);
                                            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:response.data];
                                            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                                        }
                                          error:^(LISDKAPIError *apiError) {
                                              NSLog(@"error called %@", apiError.description);
                                              CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:apiError.description];
                                              [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                                          }];
}

-(void)saveAccessToken {
    NSString* lastAccessTokenString = [[LISDKSessionManager sharedInstance] session].accessToken.serializedString;
    [[NSUserDefaults standardUserDefaults] setObject:lastAccessTokenString forKey:@"lastAccessTokenString"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(LISDKAccessToken*)lastAccessToken {
    NSString *lastAccessTokenString = [[NSUserDefaults standardUserDefaults] stringForKey:@"lastAccessTokenString"];
    if (lastAccessTokenString) return [LISDKAccessToken LISDKAccessTokenWithSerializedString:lastAccessTokenString];
    return nil;
}

-(void)createSessionWithAuth:(CDVInvokedUrlCommand*)command{
        
    AuthSuccessBlock success = ^(NSString *returnState) {
        NSLog(@"%s","success called!");
        LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
        if ([session isValid]) {
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:[session.accessToken description]];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        } else {
            CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:returnState];
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }
    };
    
    [LISDKSessionManager createSessionWithAuth:[NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, LISDK_EMAILADDRESS_PERMISSION, nil]
                                         state:@"some state"
                        showGoToAppStoreDialog:YES
                                  successBlock:success
                                    errorBlock:^(NSError *error) {
                                        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:error.description];
                                        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
                                    }
     ];
    
}


-(void)hasLinkedInApp:(CDVInvokedUrlCommand*)command{ 
    UIApplication *application = [UIApplication sharedApplication];
    NSURL *URL = [NSURL URLWithString:@"linkedin://"];
    BOOL lnInstalled = [application canOpenURL:URL];
    CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsBool:lnInstalled];
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}



@end
