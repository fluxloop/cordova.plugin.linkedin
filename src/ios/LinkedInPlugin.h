//
//  Created by fluxloop on 18/08/2016
//

#import <linkedin-sdk/LISDK.h>
#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>
#import <Cordova/CDV.h>

@interface LinkedInPlugin  : CDVPlugin {}

- (void)createSessionWithAuth:(CDVInvokedUrlCommand*)command;
- (void)viewMyProfile:(CDVInvokedUrlCommand*)command;
- (void)viewMemberProfile:(CDVInvokedUrlCommand*)command;
- (void)getProfile:(CDVInvokedUrlCommand*)command;
- (void)hasLinkedInApp:(CDVInvokedUrlCommand*)command;

@end
